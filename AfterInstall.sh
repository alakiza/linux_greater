#!/bin/bash

sudo apt update
sudo apt upgrade
sudo apt install git mc curl wget zsh htop iftop ethtool ncdu tmux net-tools nload

cd
mkdir ohmyzsh_install 
cd ./ohmyzsh_install
sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sh ./install.sh
